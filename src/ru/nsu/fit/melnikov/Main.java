package ru.nsu.fit.melnikov;
import java.io.*;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Stack;
public class Main {
    public static void main(String[] args) {
        Scanner scanner;
        if (args.length > 0) {
            try {
                scanner = new Scanner(new FileInputStream(args[0]));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return;
            }
        } else {
            scanner = new Scanner(System.in);
        }
        String buf;
        HashMap<String, Double> myDefined = new HashMap<>();
        Stack<Double> stack = new Stack<>();
        Factory factory = new Factory();
        while (scanner.hasNextLine()) {
            buf = scanner.nextLine();
            String[] arguments = buf.split(" ");
            if(arguments[0].equals("EXIT"))
                return;
            Command command = factory.getCommand(arguments[0]);
            if(command == null)
                System.out.println("wrong command");
            else
                command.todo(stack, myDefined, arguments);
        }
    }
}
