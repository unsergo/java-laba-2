package ru.nsu.fit.melnikov;
        import java.io.IOException;
        import java.io.InputStream;
        import java.util.HashMap;
        import java.util.Map;
        import java.util.Properties;
public class Factory {
    private Map<String, Command> commandMap = new HashMap<>();
    Factory() {
        Properties properties = new Properties();
        try (InputStream inputStream = Factory.class.getResourceAsStream("commands.properties")) {
            properties.load(inputStream);
        } catch (IOException e) {
            System.err.println("failed to load properties");
        }
        for (String key : properties.stringPropertyNames()) {
            try {
                Class cl = Class.forName(properties.getProperty(key));
                commandMap.put(key, (Command)cl.newInstance());
            } catch (Exception e) {
                System.err.println("failed to load properties");
            }
        }
    }
    public Command getCommand(String commandName) {
        return commandMap.get(commandName);
    }
}
