package ru.nsu.fit.melnikov.commandsForCalculation;
import ru.nsu.fit.melnikov.Command;
import java.util.HashMap;
import java.util.Stack;
public class Sqrt implements Command {
    public void todo(Stack<Double> myValue, HashMap<String, Double> myDefined, String[] arguments) {
        if (myValue.size() == 0){
            System.out.println("stack is empty");
        }
        else
        if ( myValue.peek()>=0) {
            myValue.push(Math.sqrt(myValue.pop()));
        } else{
            System.err.println("negativ radic");
        }
    }
}
