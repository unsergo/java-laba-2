package ru.nsu.fit.melnikov.commandsForCalculation;

import ru.nsu.fit.melnikov.Command;

import java.util.HashMap;
import java.util.Stack;

public class Pop implements Command {

    public void todo(Stack<Double> myValue, HashMap<String, Double> myDefined, String[] arguments) {
        try {
            myValue.pop();
        }catch (java.util.EmptyStackException e) {
            System.err.println("stack is empty");
        }
    }
}
