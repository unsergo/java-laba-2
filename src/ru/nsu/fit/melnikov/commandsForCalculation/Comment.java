package ru.nsu.fit.melnikov.commandsForCalculation;
import ru.nsu.fit.melnikov.Command;
import java.util.HashMap;
import java.util.Stack;
public class Comment implements Command {
    public void todo(Stack<Double> myValue, HashMap<String, Double> myDefined, String[] arguments) {
        for (String word : arguments ) {
            System.out.print(word+" ");
        }
        System.out.println("");
    }
}
