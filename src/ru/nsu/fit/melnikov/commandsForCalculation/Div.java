package ru.nsu.fit.melnikov.commandsForCalculation;
import ru.nsu.fit.melnikov.Command;

import java.util.HashMap;
import java.util.Stack;
public class Div implements Command {
    public void todo(Stack<Double> myValue, HashMap<String, Double> myDefined, String[] arguments) {
        double tmp;
        if (myValue.size() > 1) {
            double firstValue = myValue.pop();
            double secondValue = myValue.pop();
            if (secondValue != 0.0) {
                tmp = firstValue / secondValue;
                myValue.push(tmp);
            } else {
                System.err.println("division by zero");
                myValue.push(secondValue);
                myValue.push(firstValue);
            }
        }
        else{
            System.err.println("few arguments");
        }
    }
}
