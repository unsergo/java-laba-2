package ru.nsu.fit.melnikov.commandsForCalculation;
import ru.nsu.fit.melnikov.Command;
import java.util.HashMap;
import java.util.Stack;
public class Push implements Command {
    public void todo(Stack<Double> myValue, HashMap<String, Double> myDefined, String[] arguments) {
        try {
            if (myDefined.containsKey(arguments[1]))
                myValue.push(myDefined.get(arguments[1]));
            else
                myValue.push(Double.valueOf(arguments[1]));
        }
        catch (java.lang.ArrayIndexOutOfBoundsException f) {
            System.err.println("failed to push argument");
        }
    }
}
