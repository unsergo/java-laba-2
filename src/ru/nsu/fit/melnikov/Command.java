package ru.nsu.fit.melnikov;

import java.util.HashMap;
import java.util.Stack;

public interface Command {
    void todo(Stack<Double> myValue, HashMap<String,Double> myDefined, String[] arguments);
}
